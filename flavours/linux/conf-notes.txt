
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Supported targets:
- oniro-image-base
- oniro-image-base-dev
- oniro-image-base-tests
- oniro-image-extra
- oniro-image-extra-dev
- oniro-image-extra-tests

Supported machines (first is the default):
- qemux86-64
- qemux86
- seco-intel-b68 (SECO SBC-B68)
- seco-imx8mm-c61-2gb (SECO SBC-C61 2GB DRAM)
- seco-imx8mm-c61-4gb (SECO SBC-C61 4GB DRAM)
- raspberrypi4-64

MACHINE variable can be set up in conf/local.conf file under build directory
or via command line, e.g.:

    $ MACHINE=<supported_machine> bitbake <target>

You can also run generated qemu images with a command:

    $ runqemu qemux86-64 qemuparams="-nographic" oniro-image-base wic ovmf

